from orator.migrations import Migration


class CreatePostsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('posts') as table:
            table.increments('id')
            table.string('title')
            table.string('slug')
            table.string('image').nullable()
            table.text('body')

            table.string('meta_title')
            table.string('meta_description')
            table.enum('post_type', ['snippets', 'blog'])

            table.integer('author_id').unsigned()
            table.foreign('author_id').references('id').on('users')

            table.timestamp('published_date').nullable()
            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('posts')
