""" Base Database Seeder Module """

from orator.seeds import Seeder

from UsersTableSeeder import UsersTableSeeder
from PostTableSeeder import PostTableSeeder


class DatabaseSeeder(Seeder):

    def run(self):
        """Run the database seeds."""
        self.call(UsersTableSeeder)
        self.call(PostsTableSeeder)
