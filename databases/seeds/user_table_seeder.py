from orator.seeds import Seeder

from masonite.helpers import password as bcrypt_password


class UserTableSeeder(Seeder):

    def run(self):
        """Run the database seeds."""
        password = bcrypt_password('raj-13579')
        self.db.table('users').insert({
            'name': 'vaibhavmule',
            'email': 'vaibhavmule135@gmail.com',
            'password': password
        })