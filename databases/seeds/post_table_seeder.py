from orator.seeds import Seeder


class PostTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.db.table('posts').insert({
            'title': 'How to Read CSV File in Python',
            'author_id': 1,
            'body': 'here we go',
            'image': '',
            'slug': 'how-to-read-csv-file-python',
            'meta_title': 'How to Read CSV File in Python',
            'meta_description': 'How to Read CSV File in Python',
            'post_type': 'snippet',
        })

