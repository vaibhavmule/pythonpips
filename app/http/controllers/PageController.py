"""A PageController Module"""

from masonite.view import View
from masonite.request import Request

import pendulum

from app.Post import Post


class PageController:
    """PageController."""

    def index(self, view: View, request: Request):
        snippets = Post.where('post_type', 'snippets').where(
                'published_date', '<', pendulum.now()).order_by(
                'published_date', 'desc').take(5).get()
        posts = Post.where('post_type', 'blog').where(
                'published_date', '<', pendulum.now()).order_by(
                'published_date', 'desc').take(5).get()
        return view.render('page/index', {'posts': posts, 'snippets': snippets })
