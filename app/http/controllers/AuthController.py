"""AuthController Module """

from masonite.auth import Auth
from masonite.request import Request
from masonite.view import View


class AuthController:
    """AuthController."""

    def __init__(self):
        """AuthController Constructor."""
        pass

    def index(self, request: Request, view: View):
        if request.user():
            return request.redirect('/posts/new')
        return view.render('auth/login', {'Auth': Auth(request)})

    def login(self, request: Request):
        if Auth(request).login(request.input('email'), request.input('password')):
            return request.redirect('/posts/new')
        return request.redirect('/login')

    def logout(self, request: Request):
        Auth(request).logout()
        return request.redirect('/login')
