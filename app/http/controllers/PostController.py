"""A PostController Module"""

from masonite.view import View
from masonite.request import Request
from masonite import Upload

import markdown
import pendulum
from slugify import slugify

from app.Post import Post
from app.User import User


class PostController:
    """PostController."""

    def index(self, view: View, request: Request):
        post_type = request.param('post_type')
        posts = Post.order_by('published_date', 'desc').get()

        if post_type:
            posts = Post.where('post_type', post_type).order_by('published_date', 'desc').get()
        return view.render('post/index', {'posts': posts})

    def post_type(self, view: View, request: Request):
        page = int(request.input('page')) or 1
        post_type = request.param('post_type') or 'snippets'
        post_type_subtitle = 'Daily byte size snippets to master Python programming language.'

        if not post_type == 'snippets':
            post_type_subtitle = f'{post_type.capitalize()} is coming soon'

        posts = Post.where('post_type', post_type).where('published_date', '<', pendulum.now()).where_not_null('published_date').order_by('published_date', 'desc').paginate(5, page)
        return view.render('post/post_type', {'posts': posts, 'post_type': post_type, 'post_type_subtitle': post_type_subtitle})

    def new(self, view: View):
        return view.render('post/new')

    def create(self, request: Request, upload: Upload):
        post_type = request.input('post_type')
        post = Post(
            title=request.input('title'),
            body=request.input('body'),
            image=upload.driver('cloudinary').store(
                request.input('image'),
                post_type)['secure_url'],
            author_id=request.user().id,
            slug=request.input('slug'),
            meta_title=request.input('title'),
            meta_description='',
            post_type=post_type,
        )
        post.save()
        return request.redirect('/posts')

    def show(self, view: View, request: Request):
        post_type = request.param('post_type')
        post = Post.where('slug', request.param('slug')).first_or_fail()
        post.body = markdown.markdown(post.body)
        return view.render('post/show', {'post': post})

    def edit(self, view: View, request: Request):
        post = Post.where('id', request.param('id')).first_or_fail()
        return view.render('post/edit', {'post': post})

    def update(self, request: Request):

        post = Post.where('id', request.param('id')).first_or_fail()

        post.title = request.input('title')
        post.body = request.input('body')
        post.slug = request.input('slug')
        post.image = request.input('image')
        post.meta_title = request.input('meta_title')
        post.meta_description = request.input('meta_description')
        post.post_type = request.input('post_type')
        post.published_date = request.input('published_date')

        post.save()

        post = Post.where('id', request.param('id')).first_or_fail()

        return request.redirect(f"/{post.post_type}/{post.slug}")

    def delete(self, request: Request):
        post = Post.where('id', request.param('id')).first_or_fail()
        post.delete()
        return request.redirect('/posts')