"""A MetaController Module."""

from masonite.view import View
from masonite.request import Request

from app.Post import Post


class MetaController:
    """MetaController
    """

    def sitemap(self, view: View, request: Request):
        posts = Post.all()
        request.header('Content-Type', 'application/xml')
        return view.render('meta/sitemap', {'posts': posts})

    def feed(self, view: View, request: Request):
        posts = Post.all()
        request.header('Content-Type', 'application/xml')
        return view.render('meta/feed', {'posts': posts})