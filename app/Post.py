''' A Post Model '''
from config.database import Model
from orator.orm import belongs_to
from app.User import User


class Post(Model):
    __table__ = 'posts'

    __fillable__ = ['title',
                    'author_id',
                    'body',
                    'image',
                    'slug',
                    'meta_title',
                    'meta_description',
                    'post_type']

    @belongs_to('author_id', 'id')
    def author(self):
        return User
