""" Web Routes """

from masonite.routes import Get, Post


ROUTES = [
    # Pages
    Get().route('/', 'PageController@index'),
    Get().view('/about', 'page/about'),
    Get().view('/contact', 'page/contact'),
    Get().view('/privacy', 'page/privacy'),

    # Meta files
    Get().route('/sitemap.xml', 'MetaController@sitemap'),
    Get().route('/feed', 'MetaController@feed'),

    # Authentication
    Get().route('/login', 'AuthController@index'),
    Post().route('/login', 'AuthController@login'),
    Get().route('/logout', 'AuthController@logout'),

    # Posts
    Get().route('/posts', 'PostController@index'),
    Post().route('/posts', 'PostController@create'),
    Get().route('/posts/new', 'PostController@new').middleware('auth'),
    Get().route('/posts/@id:int/edit', 'PostController@edit'),
    Post().route('/posts/@id:int', 'PostController@update'),
    Get().route('/posts/@id:int/delete', 'PostController@delete'),
    Get().route('/posts/@post_type', 'PostController@index'),
    Get().route('/@post_type', 'PostController@post_type'),
    Get().route('/@post_type/@slug', 'PostController@show'),
]
